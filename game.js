(function() {
  function $(id) {
    return document.getElementById(id);
  }

  function rnd(min, max) {
    return ~~(Math.random() * (max - min + 1)) + min;
  }

  function showScreen(el) {
    el.style.display = "block";
  }

  function feminize() {
    return "<span>"
      + quotes[rnd(0, quotes.length-1)]
      + "</span>";
  }
  
  // var hammer = $("hammer");
  var gameOverScreen = $("gameOverScreen");
  var winScreen = $("winScreen");
  gameOverScreen.style.display = "none";
  winScreen.style.display = "none";
  
  // $("container").addEventListener("mousemove", function(e) {
  //   var event = e || window.event;
  //   hammer.style.left = event.clientX - 100 + "px";
  //   hammer.style.top = event.clientY - 100 + "px";
  // }, false);
  
  // $("container").addEventListener("mousedown", function(e) {
  //   hammer.className = "hammer-hit";
  // }, false);

  // $("container").addEventListener("mouseup", function(e) {
  //   hammer.className = "";
  // }, false);

  $("start").addEventListener("click", startGame, false);

  function startGame() {
    $("startScreen").style.display = "none";
    gameOverScreen.style.display = "none";
    winScreen.style.display = "none";
    inGame = true;
  }
  
  var inGame = false;
  var moles = [];
  var moleIsOut = false;
  var levels = 5;
  var maxLevels = 5;
  var misses = 0;
  var score = 0;
  var playtime = {
    rounds: 5,
    uptime: 7
  };
  var blocks = ["nw", "n", "ne", "w", "c", "e", "sw", "s", "se"];
  var level = $("level");
  var lives = $("lives");
  var scores = $("score");

  function countDownRounds(current) {
    if (current.rounds == 1) {
      levels--;
      return {
        rounds: 5,
        uptime: 7
      };
    }
    current.rounds-=1;
    current.uptime-=1;
    return current;
  }

  function removeMole(mole) {
    var index = moles.indexOf(mole);
    if (index > -1) {
      moles.splice(index, 1);
    }
  }
  
  function selectMoles() {
    // return array of moles
    var arr = [];
    var selectedBlock = blocks[rnd(0, blocks.length-1)];
    arr.push(selectedBlock);
    var maxMoles = maxLevels - levels; // max levels - current levels
    while(maxMoles) {
      selectedBlock = blocks[rnd(0, blocks.length-1)];
      var lookup = arr.filter(function(item) {
        return item == selectedBlock;
      });
      if (lookup.length === 0) {
        maxMoles--;
        arr.push(selectedBlock);
      }
    }
    return arr;
  }

  function selectMole() {
    return blocks[0];
  }
  
  function getMoleOut(mole) {
    console.log("release the mole!!", mole);
    moleIsOut = true;
    var el = $(mole);
    var moleEl = document.createElement("div");
    moleEl.className = "sjw";
    var quoteEl = document.createElement("div");
    quoteEl.className = "quote";
    quoteEl.innerHTML = feminize();
    moleEl.appendChild(quoteEl);
    var arrowEl = document.createElement("div");
    arrowEl.className = "arrow";
    moleEl.appendChild(arrowEl);
    el.appendChild(moleEl);
    moleEl.addEventListener("click", function(e) {
      // success
      // remove current mole from array
      removeMole(mole);
      el.removeChild(moleEl);
      clearInterval(timer);
      score += 10;
    });
    var counter = 0;
    var timer = setInterval(function() {
      // fail
      if (counter > playtime.uptime) {
        // remove current mole from array
        misses+=1;
        removeMole(mole);
        el.removeChild(moleEl);
        clearInterval(timer);
      } else {
        counter++;
      }
    }, 1000);
  }

  function getMolesOut() {
    moles = selectMoles();
    moles.forEach(function(mole) {
      getMoleOut(mole);
    });
  }
  
  function molesOnField() {
    return moles.length !== 0;
  }

  function gameOver() {
    return misses >= 3;
  }

  function gameWin() {
    return levels == 0;
  }

  var gameLoop = setInterval(function() {
    if (!inGame) {
      return;
    }
    if (gameOver()) {
      inGame = false;
      clearInterval(gameLoop);
      showScreen(gameOverScreen);
      return;
    }
    if (gameWin()) {
      inGame = false;
      clearInterval(gameLoop);
      showScreen(winScreen);
      return;
    }
    level.innerHTML = maxLevels - levels + 1;
    lives.innerHTML = 3 - misses;
    scores.innerHTML = score;
    if (!molesOnField() && inGame) {
      getMolesOut();
      playtime = countDownRounds(playtime);
    }
    console.log("round", playtime.rounds,
                "uptime", playtime.uptime,
                "misses", misses);
  }, 1000);
  
}());
